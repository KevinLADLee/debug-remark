# 轮廓的矩
在OpenCV中，可以很方便的计算多边形区域的3阶特征矩，opencv中的矩主要包括以下几种：空间矩，中心矩和中心归一化矩。


```cpp
class Moments { 
 public: ...... 
   // 空间矩 
   double m00, m10, m01, m20, m11, m02, m30, m21, m12, m03; 
   // 中心矩 
   double mu20, mu11, mu02, mu30, mu21, mu12, mu03; 
   // 中心归一化矩 
   double nu20, nu11, nu02, nu30, nu21, nu12, nu03; 
}

```

空间矩的公式为：

![c1](http://chart.apis.google.com/chart?chf=bg,s,fffff0&cht=tx&chl=m_%7Bji%7D%20%3D%20%5Csum_%7Bx%2Cy%7D(array(x%2C%20y)%20%5Ccdot%20x%5E%7Bj%7D%20%5Ccdot%20y%5Ei))

<!-- $$m_{ji} = \sum_{x,y}(array(x, y) \cdot x^{j} \cdot y^i)$$ -->

可以知道，对于01二值化的图像，m00即为轮廓的面积。中心矩的公式为：

![c2](http://chart.apis.google.com/chart?chf=bg,s,fffff0&cht=tx&chl=mu_%7Bji%7D%20%3D%20%5Csum_%7Bx%2Cy%7D(array(x%2C%20y)%20%5Ccdot%20(x-%5Cbar%7Bx%7D)%5E%7Bj%7D%20%5Ccdot%20(y-%5Cbar%7By%7D)%5Ei))


<!-- $$mu_{ji} = \sum_{x,y}(array(x, y) \cdot (x-\bar{x})^{j} \cdot (y-\bar{y})^i)$$ -->

其中：

![c3](http://chart.apis.google.com/chart?chf=bg,s,fffff0&cht=tx&chl=%5Cbar%7Bx%7D%20%3D%20%5Cfrac%7Bm_%7B10%7D%7D%7Bm_%7B00%7D%7D%2C%5Cbar%7By%7D%20%3D%20%5Cfrac%7Bm_%7B01%7D%7D%7Bm_%7B00%7D%7D)

<!-- $$\bar{x} = \frac{m_{10}}{m_{00}},\bar{y} = \frac{m_{01}}{m_{00}}$$ -->


