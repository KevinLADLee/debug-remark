# Karto源码分析

## 命名规范

* `m_`代表私有变量
* `p`代表指针类型
* `r`代表引用类型
* `kr_`代表Karto为跨平台使用的type define类型，

## 基本结构

mapper含前端、回环检测等

solver主要负责后端求解



## 激光雷达回调

SlamKarto::laserCallback(const sensor_msgs::LaserScan::ConstPtr& scan)

激光雷达消息的订阅

getLaser

判断是否已经初始化过当前对应的激光雷达

addScan

添加scan到激光帧序列



## 激光雷达初始化

```c++
SlamKarto::getLaser(const sensor_msgs::LaserScan::ConstPtr& scan)
```

1. 利用find函数搜索是否有当前frame_id对应的激光雷达对象
2. 若无则进入激光雷达对象初始化
3. 通过tf，拿到激光雷达相对base_frame的变换
4. 判断雷达是否倒装
5. 创建karto::LaserRangeFinder对象，包含激光雷达基本信息，记录在激光雷达对象对map队列中
6. 添加该激光雷达到dataset中



## 添加激光帧

```c++
SlamKarto::addScan(karto::LaserRangeFinder* laser, const sensor_msgs::LaserScan::ConstPtr& scan, karto::Pose2& karto_pose)
```

1. 通过tf拿里程计信息（`base_frame`相对`odom`的变换）
2. 通过激光雷达点云数据创建`karto::LocalizedRangeScan`对象
3. `LocalizedRangeScan`对象的`OdomPose`和`CorrectedPose`（校正Pose）初始设定为里程计的pose
4. 调用mapper的Process函数，进行前端匹配并尝试检测回环
5. Process成功后，拿到校正后的pose（`CorrectedPose`），更新map到odom的tf变换
6. 添加该激光雷达帧对象到dataset

## Mapper->Process

1. 通过`RangeScan`对象拿`LaserRangeFinder`对象
2. 如果Mapper未初始化，进行mapper的初始化
3. 通过m_pMapperSensorManager拿到最后一个RangeScan对象的指针pLastScan
4. 计算pLastScan中OdomPose到校正Pose的变换，并将当前Scan对象的OdomPose应用此变换到对应的校正Pose上
5. 如果未移动足够的距离或者转动足够的角度（参数MinimumTravelDistance和MinimumTravelHeading），则跳过当前帧
6. 若激光帧非第一帧，则进行调用MatchScan进行激光帧匹配，估计出bestPose和协方差
7. 更新传感器pose（pScan->SetSensorPose）
8. 添加当前scan到SensorManager（m_pMapperSensorManager->AddScan(pScan))
9. 如果进行了MatchScan则更新Pose Graph，添加顶点和边，并添加scan到RunningScan
10. 尝试回环检测（m_pGraph->TryCloseLoop）
11. 设定当前scan为SensorMangager里面的LastScan



## MatchScan

```c++
MatchScan(pScan, m_pMapperSensorManager->GetRunningScans(pScan->GetSensorName()), bestPose, covariance);
```

